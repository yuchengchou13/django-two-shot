from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    create_receiptForm,
    create_categoryForm,
    create_accountForm,
)

# POST
# create form object "variable = something"
# check if form is valid (filled out correctly and fully)
# if not valid, then re-render form with userinfo and error message
# if valid, data is "cleaned" by django into variable.cleaned_data['']
# check if password == password_confirm
# if not valid, double check and reenter until valid
# if valid, make a new user then login and redirect to the mainpage

# GET
# create instance of blank form
# put blank form into context dictionary

# render page with form


@login_required(login_url="/accounts/login/")
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipts_list.html", context)


@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = create_receiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = create_receiptForm()
    context = {
        "form": form,
    }
    form.fields["category"].queryset = ExpenseCategory.objects.filter(
        owner=request.user
    )
    form.fields["account"].queryset = Account.objects.filter(
        owner=request.user
    )
    return render(request, "receipts/create_receipt.html", context)


@login_required(login_url="/accounts/login/")
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "categories": categories,
    }
    return render(request, "receipts/category_list.html", context)


@login_required(login_url="/accounts/login/")
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/account_list.html", context)


@login_required(login_url="/accounts/login/")
def create_category(request):
    if request.method == "POST":
        form = create_categoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = create_categoryForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required(login_url="/accounts/login/")
def create_account(request):
    if request.method == "POST":
        form = create_accountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = create_accountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
