from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# @admin.register(model name)
# class model nameAdmin(admin.ModelAdmin)
# list the display = ("")


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    display = (
        "name",
        "owner",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    display = (
        "name",
        "number",
        "owner",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )
